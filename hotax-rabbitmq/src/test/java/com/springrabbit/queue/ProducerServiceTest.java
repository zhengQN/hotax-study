package com.springrabbit.queue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public class ProducerServiceTest {

	@Autowired
	ProducerService producerService;
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	
	private static final String EXCHAMGE_NAME = "";
	private static final String QUEUE_NAME = "springrabbit.queue";
	
	
	@Test
	public void sendTest() {
		String message = "order_20190814181935646_349";
		amqpTemplate.convertAndSend(message);
//		producerService.send(EXCHAMGE_NAME, QUEUE_NAME, message);
	}
	
	
}
