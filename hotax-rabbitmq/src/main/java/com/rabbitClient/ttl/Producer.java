package com.rabbitClient.ttl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {

	public static final String EXCHANGE_NAME = "rabbitClient.dlx.exchange";
//	public static final String QUEUE_NAME = "rabbitClient.dlx.exchange";
	public static final String ROUTING_KEY = "dlx.save";
	public static final String HOST = "192.168.1.219";
	public static final String USER = "guest";
	public static final String PASSWORD = "guest";
	public static final int PORT = 5672;
	public static final String VIRTUAL_HOST = "test";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setPort(PORT);
		factory.setUsername(USER);
		factory.setPassword(PASSWORD);
		factory.setVirtualHost(VIRTUAL_HOST);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		String message = "hello RabbitMQ";
	
		// 发送消息到队列中
		String newMessage;
		for (int i = 0; i < 10; i++) {
			newMessage = message + i;
			AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
					.deliveryMode(2)
					.expiration("100000")
					.build();
			channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, properties, newMessage.getBytes("UTF-8"));
		}

		// 关闭通道和连接
		channel.close();
		connection.close();
	}
}
