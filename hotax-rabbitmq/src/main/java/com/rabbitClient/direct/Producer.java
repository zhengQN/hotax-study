package com.rabbitClient.direct;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {

	private static final String[] routingKeys = new String[]{"info" ,"warning"};
	public static final String EXCHANGE_NAME = "rabbitClient.routing";
	public static final String HOST = "192.168.1.219";
	public static final String USER = "guest";
	public static final String PASSWORD = "guest";
	public static final int PORT = 5672;
	public static final String VIRTUAL_HOST = "test";
	
	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory= new ConnectionFactory();
		factory.setHost(HOST);
		factory.setPort(PORT);
		factory.setUsername(USER);
		factory.setPassword(PASSWORD);
		factory.setVirtualHost(VIRTUAL_HOST);
		
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		/*第一个参数为队列名
		    *第二个参数为是否持久化（队列在服务器重启后继续存在）
		    *第三个参数为是否独占队列
		    *第四个参数为是否自动删除队列（服务器不再使用时将其删除）
		 *第五个为参数队列的其他属性
		**/
		//路由，消息发送给交换机的时候就指定了由哪个队列来接受
		channel.exchangeDeclare(EXCHANGE_NAME, "direct");
		String message ="hello RabbitMQ";
		//发送消息到队列中
		String newMessage;
		for(int i=0;i<10;i++){
			newMessage = message+i;
			channel.basicPublish(EXCHANGE_NAME, routingKeys[0], null, newMessage.getBytes("UTF-8"));
		}
		for(int i=0;i<5;i++){
			newMessage = message+i;
			channel.basicPublish(EXCHANGE_NAME, routingKeys[1], null, newMessage.getBytes("UTF-8"));
		}
		//关闭通道和连接
		channel.close();
		connection.close();
	}
}
