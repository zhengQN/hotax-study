package com.springrabbit.queue;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {

	@Autowired
	private AmqpTemplate amqpTemplate;
	
	public void send(String exchange, String routingKey, Object message){
		amqpTemplate.convertAndSend(exchange, routingKey, message);
	}
}
