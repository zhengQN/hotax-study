package com.springrabbit.queue;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.Channel;

@Service
public class OrderOverTimeConsumer implements ChannelAwareMessageListener{

	
	
	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		// 获取信息
		String orderSn = new String(message.getBody());

		//对信息进行业务处理
		System.out.println("取消订单操作完成"+orderSn);
		//手动确认消息消费
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);

	}

}
