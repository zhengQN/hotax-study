# spring-data-redis 实现延迟任务

#### 介绍
集合spring-data-redis
并且实现sub/pub

1、需要实现key过期通知  ——需在redis.conf中配置    notify-keyspace-events EX  
2、channelTopic 配置value 为   __keyevent@0__:expired  
	此名称为redis固定的名称  
            标识过期key的消息通知  
    0标识db0 根据自己的dbindex选择格式的数字  
3、这样，db0中过期的key就会进入到 __keyevent@0__ 的队列中。  
4、配置messageListener来监听 __keyevent@0__ 的队列中的消息




#### 注意事项
spring-data-redis 和 jedis版本对应问题，如果版本不对应，会报错

##### 版本对应表
spring-data-redis版本                   |       Jedis版本  
1.5.2.RELEASE             |       2.7.3  
1.6.0.RELEASE             |       2.7.2  2.7.3  
1.6.2.RELEASE             |       2.8.0  
1.8.1.RELEASE             |       2.9.0  
1.8.4.RELEASE             |       2.9.0  



