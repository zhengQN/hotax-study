package com.hotax.redis.model;

import lombok.Data;

@Data
public class Student {
	
	private String name;
	private Integer age;
	private Integer sex;
	
}
