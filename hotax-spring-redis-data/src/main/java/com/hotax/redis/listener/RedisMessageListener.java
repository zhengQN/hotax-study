package com.hotax.redis.listener;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * 配置redis消息监听器
 * 获取redis中的消息并进行处理
 *
 */
public class RedisMessageListener implements MessageListener{

	/**
	 * onMessage 消息处理
	 * 	message：完整的消息（频道的消息。以及消息的具体内容）
	 * 	pattern：获取的频道信息
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		String channel = new String(message.getChannel());
		String key = new String(message.getBody());
		System.out.println("从channel为"+channel+"中获取一条新的消息，消息的key为："+key);
		
	}

}
