package com.hotax.coupon.test;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-redis.xml")
public class CouponTest {

	@Autowired
	private RedisTemplate redisTemplate;
	
	/**
	 * 1、创建一个优惠券
	 * 2、保存到数据库中
	 * 3、保存到redis服务器中，设置超时时间
	 */
	@Test
	public void testGetCoupon() {
		//优惠券超时时间
		int timeout = 1;
		String couponId = "coupon01";
		
		//将优惠券设置到redis缓存中，并且设置超时时间
		redisTemplate.opsForValue().set(couponId, couponId, timeout, TimeUnit.MINUTES);
		
	}
	
}
