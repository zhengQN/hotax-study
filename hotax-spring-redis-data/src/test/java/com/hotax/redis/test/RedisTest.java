package com.hotax.redis.test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hotax.redis.model.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring-redis.xml")
public class RedisTest {
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Test
	public void testString() {
		//redsi中放入string类型的值
		redisTemplate.opsForValue().set("test", "testRedis");
		//获取字符串
		String str = redisTemplate.opsForValue().get("test");
		System.out.println(str);
		//字符串后缀添加
		redisTemplate.opsForValue().append("test", "123");
		str = redisTemplate.opsForValue().get("test");
		System.out.println("append之后的test值：" + str);
		//设置过期时间
		redisTemplate.expire("test", 100, TimeUnit.SECONDS);
		
		//使用map一次存放多个值
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "张三");
		map.put("age", "18");
		map.put("sex", "2");
		redisTemplate.opsForValue().multiSet(map);
		
		//使用map一次存放多个值
		Set<String> keys = new HashSet<String>();
		keys.add("name");
		keys.add("age");
		keys.add("sex");
		List<String> list = redisTemplate.opsForValue().multiGet(keys);
		System.out.println("以map放入到redis中之后的："+list);
		
		//放入一个对象,转换成json字符串存入到redis中
		Student student = new Student();
		student.setName("王五");
		student.setAge(3);
		student.setSex(2);
		String string = JSON.toJSONString(student);
		redisTemplate.opsForValue().set("student", string);
		String jsonObject = redisTemplate.opsForValue().get("student");
		//两种解析json的方式
		JSONObject object = JSON.parseObject(jsonObject);
		System.out.println("age=="+object.get("age"));
		Student stu = JSON.parseObject(jsonObject, Student.class);
		System.out.println(stu.toString());
	}
	
	@Test
	public void testHash() {
		//设置hash存储值
		redisTemplate.opsForHash().put("student-001", "name", "张三");
		redisTemplate.opsForHash().put("student-001", "age", "18");
		redisTemplate.opsForHash().put("student-001", "sex", "2");
		
		//存放一个map类型
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "李四");
		map.put("age", "19");
		map.put("sex", "1");
		redisTemplate.opsForHash().putAll("student-002", map);
		
		//获取hash值
		String name = (String) redisTemplate.opsForHash().get("student-001", "name");
		System.out.println("获取第一个hash中的值"+name);
		String name2 = (String) redisTemplate.opsForHash().get("student-002", "name");
		System.out.println("获取第二个hash中的值"+name2);
		
		//根据key删除缓存  
		redisTemplate.delete("student-001");
		redisTemplate.delete("student-002");
	}
	
	
	@Test
	public void testSet() {
		//插入值
		redisTemplate.opsForSet().add("testSet", "aa","bb","bb");
		//移除并返回集合中的一个随机元素
//		String pop = redisTemplate.opsForSet().pop("testSet");
//		System.out.println(pop);
		//返回所的成员
		Set<String> members = redisTemplate.opsForSet().members("testSet");
		System.out.println(members);
	}
	
	@Test
	public void testZset() {
		
	}
	
	@Test
	public void testList() {
		
	}
	
	
}
