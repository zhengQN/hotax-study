package com.hotax.redis.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RedisTest02 {
	/**
	 * 启动main函数后，在redis客户端汇中，redis-cli中 输入命令
	 * publish ITCAST "test"
	 * 则会在控制台中输出
	 */
	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring-redis.xml");
	}
}
